<?php

// require_once(dirname(__FILE__) . "/database.php");

class API
{
	private $db;
	private $config;
	private $json_data;

	/**
	 * Конструктор класса
	 * создает экземпляр класса для подключения к БД
	 * применяет настройки
	*/
	public function __construct($config_db, $config_main)
	{
		$this->config = $config_main;
	}

	/**
	 * Проверяет есть ли уже такой параметр фильтра
	*/
	private function checkInclusionFilterParams($obj, $slug)
	{
		foreach ($obj as $key => $value) {
			if ($value->slug == $slug) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Читает json файл с данными
	*/
	public function readJson($assoc, $name) {
		$file_name = $this->config['json_filename'];
		if ($name) {
			$file_name = $name;
		}
		$stringData = file_get_contents($file_name);
		$json_data = json_decode($stringData, $assoc);
		return $json_data;
	}

	
}
